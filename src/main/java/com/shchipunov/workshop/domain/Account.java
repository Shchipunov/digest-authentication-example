package com.shchipunov.workshop.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import org.springframework.security.core.CredentialsContainer;

/**
 * Presents a class that holds account details
 * 
 * @author Shchipunov Stanislav
 */
@Entity
public class Account implements CredentialsContainer, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String username;
	private String password;
	@Enumerated(EnumType.STRING)
	private Role role;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public void eraseCredentials() {
		this.password = null;
	}

	@Override
	public boolean equals(Object object) {
		if (object != null && object instanceof Account) {
			Account that = (Account) object;
			return this.username.equals(that.username);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return username.hashCode();
	}
}
