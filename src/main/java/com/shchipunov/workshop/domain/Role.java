package com.shchipunov.workshop.domain;

/**
 * @author Shchipunov Stanislav
 */
public enum Role {

	ROLE_USER, ROLE_ADMINISTRATOR
}
