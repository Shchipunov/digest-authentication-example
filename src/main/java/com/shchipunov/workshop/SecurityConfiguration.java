package com.shchipunov.workshop;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.www.DigestAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.DigestAuthenticationFilter;

import com.shchipunov.workshop.domain.Account;
import com.shchipunov.workshop.repository.AccountRepository;

/**
 * Security configuration
 * 
 * @author Shchipunov Stanislav
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String KEY = "secret";
	private static final String REALM_NAME = "workshop";

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * Customized user details service  
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(new UserDetailsService() {
			@Override
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				Account account = Optional.ofNullable(accountRepository.findOne(username)).orElseThrow(
						() -> new UsernameNotFoundException("Could not find: " + username));
				return new User(account.getUsername(), account.getPassword(),
						Arrays.asList(new SimpleGrantedAuthority(account.getRole().toString())));
			}
		});
	}

	/**
	 * Main security configuration. Added digest authentication entry point and
	 * digest authentication filter.
	 */
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.exceptionHandling()
				.authenticationEntryPoint(digestAuthenticationEntryPoint())
			.and()
				.addFilter(digestAuthenticationFilter())
				.csrf().disable();
	}

	/**
	 * Registers digest authentication entry point
	 * @return
	 */
	@Bean
	public DigestAuthenticationEntryPoint digestAuthenticationEntryPoint() {
		DigestAuthenticationEntryPoint digestAuthenticationEntryPoint = new DigestAuthenticationEntryPoint();
		digestAuthenticationEntryPoint.setRealmName(REALM_NAME);
		digestAuthenticationEntryPoint.setKey(KEY);
		return digestAuthenticationEntryPoint;
	}

	/**
	 * Registers digest authentication filter
	 * @return
	 */
	@Bean
	public DigestAuthenticationFilter digestAuthenticationFilter() {
		DigestAuthenticationFilter digestAuthenticationFilter = new DigestAuthenticationFilter();
		digestAuthenticationFilter.setUserDetailsService(userDetailsService());
		digestAuthenticationFilter.setAuthenticationEntryPoint(digestAuthenticationEntryPoint());
		return digestAuthenticationFilter;
	}
}
