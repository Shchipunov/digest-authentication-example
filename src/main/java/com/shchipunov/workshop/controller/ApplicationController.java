package com.shchipunov.workshop.controller;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.shchipunov.workshop.domain.Account;
import com.shchipunov.workshop.exception.AccountNotFoundException;
import com.shchipunov.workshop.repository.AccountRepository;

/**
 * Processes client requests
 * 
 * @author Shchipunov Stanislav
 */
@RestController
public class ApplicationController {

	private final AccountRepository accountRepository;

	@Autowired
	public ApplicationController(AccountRepository accountRepository) {
		this.accountRepository = Objects.requireNonNull(accountRepository);
	}

	/**
	 * Sends welocme message
	 * 
	 * @return
	 */
	@RequestMapping(value = { "/" }, method = RequestMethod.GET, produces = "application/json")
	public String root() {
		return "Welcome to digest authentication example!";
	}

	/**
	 * Creates {@link Account}
	 * 
	 * @param account
	 * @param uriComponentsBuilder
	 * @return
	 */
	@RequestMapping(value = "/accounts", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<Account> createAccount(@RequestBody Account account,
			UriComponentsBuilder uriComponentsBuilder) {
		account = accountRepository.save(account);
		HttpHeaders httpHeaders = new HttpHeaders();
		URI location = uriComponentsBuilder
				.path("/accounts/")
				.path(String.valueOf(account.getUsername()))
				.build()
				.toUri();
		httpHeaders.setLocation(location);
		return new ResponseEntity<Account>(account, httpHeaders, HttpStatus.CREATED);
	}

	/**
	 * Reads {@link Account}
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/accounts/{id}", method = RequestMethod.GET, produces = "application/json")
	public Account readAccount(@PathVariable String id) {
		Optional<Account> accountOptional = Optional.ofNullable(accountRepository.findOne(id));
		return accountOptional
				.orElseThrow(() -> new AccountNotFoundException("Account with ID: " + id + " is not found!"));
	}

	/**
	 * Updates {@link Account}
	 * 
	 * @param account
	 */
	@RequestMapping(value = "/accounts/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateAccount(@RequestBody Account account) {
		accountRepository.save(account);
	}

	/**
	 * Deletes {@link Account}
	 * 
	 * @param id
	 */
	@RequestMapping(value = "/accounts/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAccount(@PathVariable String id) {
		accountRepository.delete(id);
	}

	/**
	 * Handles {@link AccountNotFoundException} exception
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(AccountNotFoundException.class)
	public ResponseEntity<String> handleAccountNotFoundException(AccountNotFoundException exception) {
		return new ResponseEntity<String>(exception.getMessage(), HttpStatus.NOT_FOUND);
	}
}
