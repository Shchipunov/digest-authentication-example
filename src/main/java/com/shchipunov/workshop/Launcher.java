package com.shchipunov.workshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Shchipunov Stanislav
 */
@SpringBootApplication
public class Launcher {

    public static void main(String[] arguments) {
    	SpringApplication.run(Launcher.class, arguments);
    }
}
