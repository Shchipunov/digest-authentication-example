package com.shchipunov.workshop;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URI;
import java.net.URL;
import java.util.Random;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.DigestScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.shchipunov.workshop.domain.Account;

/**
 * @author Shchipunov Stanislav
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ApplicationControllerTests {

	private static final String USER_NAME = "test";
	private static final String PASSWORD = "test";

	@Value("${local.server.port}")
	private int port;

	private URL indexPage;
	private TestRestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
		this.indexPage = new URL("http://localhost:" + port + "/");
		restTemplate = new TestRestTemplate();
	}

	@Test
	public void shouldShowWelcomeMessage() throws Exception {
		ResponseEntity<String> responseEntity = restTemplate.getForEntity(indexPage.toString(), String.class);
		assertThat(responseEntity.getBody(), equalTo("Welcome to digest authentication example!"));
	}

	@Test
	public void shouldGetAccount() throws Exception {
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(USER_NAME, PASSWORD));
		CloseableHttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider)
				.useSystemProperties().build();
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(client) {
			@Override
			protected HttpContext createHttpContext(HttpMethod httpMethod, URI uri) {
				HttpHost host = new HttpHost("localhost", port, "http");

				DigestScheme scheme = new DigestScheme();
				scheme.overrideParamter("realm", "workshop");
				scheme.overrideParamter("nonce", Long.toString(new Random().nextLong(), 36));

				AuthCache cache = new BasicAuthCache();
				cache.put(host, scheme);

				BasicHttpContext context = new BasicHttpContext();
				context.setAttribute(HttpClientContext.AUTH_CACHE, cache);
				return context;
			}
		};
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		ResponseEntity<Account> responseEntity = restTemplate.exchange(indexPage.toString() + "/accounts/" + USER_NAME,
				HttpMethod.GET, null, Account.class);

		assertThat(responseEntity.getBody().getUsername(), equalTo(USER_NAME));
	}
}
