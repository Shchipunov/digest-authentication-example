# Digest authentication example #

It's an implementation of digest access authentication (one of the agreed-upon methods a web server can use to negotiate credentials, such as username or password, with a user's web browser).

### Project description ###
The project uses:

* Spring Boot
* Spring (MVC)
* Spring Security
* Spring Data JPA
* Apache HTTP client
* Liquibase